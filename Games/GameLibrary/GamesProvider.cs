﻿using System;
using System.Linq;
using Infrastructure;

namespace GameLibrary
{
    public class GamesProvider : IGamesProvider
    {
        //private readonly AbstractGameFactory[] _games;
        private IGame _chosenGame;
        private readonly AbstractGameFactory[] _gameFactories;
        public event EventHandler<MessagesEventArgs> Messages2;
        public event EventHandler<GameFinishedEventArgs> GameFinished2;

        public GamesProvider(AbstractGameFactory[] gameFactories)
        {
            this._gameFactories = gameFactories;
        }


        public string[] GetGameNames()
        {
            return _gameFactories.Select(g => g.Name).ToArray();
        }

        //public void ChooseGame(int n)
        //{
        //    _chosenGame = _games[n];
        //}

        public void ChooseGame(string name)
        {
             var gameFactory= _gameFactories.First(g => g.Name == name);
            _chosenGame = gameFactory.CreateGameInstance();

            _chosenGame.Messages += InitiateNextEvent;
            _chosenGame.GameFinished += InitiateNextEvent2;
        }

        public bool Guess(string bet)
        {
            return _chosenGame.Guess(bet);
        }

        public int Result
        {
            get { return _chosenGame.Result; }
        }

        public string Name
        {
            get;
            set;
        }

        public void InitiateNextEvent(object oSender, MessagesEventArgs oMessagesEventArgs)
        {
            Messages2?.Invoke(this, oMessagesEventArgs);
        }

        public void InitiateNextEvent2(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        {
            oGameFinishedEventArgs.Name = Name;
            GameFinished2?.Invoke(this, oGameFinishedEventArgs);
        }
    }
}
