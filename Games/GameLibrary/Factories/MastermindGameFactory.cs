﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public class MastermindGameFactory : AbstractGameFactory
    {
        public override IGame CreateGameInstance()
        {
            return new MastermindGame();
        }
        public override string Name => "Mastermind game";
    }
}
