﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public class TimeBasedMastermindGameFactory :AbstractGameFactory
    {
        public override IGame CreateGameInstance()
        {
            return new TimeBasedMastermindGame();
        }
        public override string Name => "Mastermind game - time-based";
    }
}
