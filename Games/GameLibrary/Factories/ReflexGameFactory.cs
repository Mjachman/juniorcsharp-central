﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public class ReflexGameFactory : AbstractGameFactory
    {
        public override IGame CreateGameInstance()
        {
            return new ReflexGame();
        }
        public override string Name => "Reflex game";
    }
}
