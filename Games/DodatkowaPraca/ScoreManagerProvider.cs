﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using HighScore;
using Infrastructure;
using Ninject.Activation;

namespace DodatkowaPraca
{
    internal class ScoreManagerProvider : Provider<IScoreStrategy>
    {
        protected override IScoreStrategy CreateInstance(IContext context)
        {
            CompositeHighScoreStrategy strategy = new CompositeHighScoreStrategy();

            var selectprovider = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["ChooseSqlProvider"];

            var connectionString = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["ConnectionString"];
            var filepath = ((NameValueCollection)ConfigurationManager.GetSection("ScoreClassConfig"))["FilePath"];

            if (selectprovider == "true")
            {
                strategy.AddNewLeaf( new SqlHighScoreStrategy(connectionString, 10));
                strategy.AddNewLeaf(new FileHighScoreStrategy(filepath,10));
            }
            
            else
            {
                
               return new FileHighScoreStrategy(filepath, 10);
            }  
            
            return strategy;
            
            
            //else
            //{
                
            //    return new HighScoreManager(new Lazy<IScoreStrategy>(() => new FileHighScoreStrategy(filepath, 10)));
            //}
            
        }
    }
}
