﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;
using Ninject.Extensions.Factory;

namespace HighScore
{
    public class CompositeHighScoreStrategy : IScoreStrategy
    {
        readonly List<IScoreStrategy> _strategyList = new List<IScoreStrategy>();


        public HighscoreEntry[] List()
        {
            foreach (var strategy in _strategyList)
            {
                HighscoreEntry[] tmp;
                try
                {
                     tmp = strategy.List();
                }
                catch
                {
                    continue;
                }

                if (tmp != null && tmp.Any())
                    return tmp;
            }

            return new HighscoreEntry[0];
        }

        public void Add(HighscoreEntry entry)
        {
            foreach (var strategy in _strategyList)
            {
                try
                {
                    strategy.Add(entry);
                }
                catch
                {
                    // ignore
                }
            }
        }

        public void AddNewLeaf(IScoreStrategy strategy)
        {
            _strategyList.Add(strategy);   
        }

        public void RemoveOneLeaf(int i)
        {
            _strategyList.RemoveAt(i);
        }
    }
}
