namespace Infrastructure
{
    public interface IScoreStrategy
    {
        HighscoreEntry[] List();
        void Add(HighscoreEntry entry);
        //void GameOver(object oSender, GameFinishedEventArgs oGameFinishedEventArgs);
       // string Name { get; set; }
        //IGamesProvider GamesProvider { get; set; }
    }
}