﻿using System;
using System.Collections;
using System.Collections.Generic;
using HighScore;
using Infrastructure;
using NUnit.Framework;
using Moq;

namespace GameLibrary.Tests
{
    /// <summary>
    /// The text fixture for 
    /// </summary>
    [TestFixture]
    public class GuessGameTests
    {
        /// <summary>
        /// AddEntry_CheckIfNothingHappens_NothingHappens for 
        /// </summary>
        [Test]
        public void GuessGame_NonNumberString_ArgumentOutOfRange()
        {
            var game = new GuessGame();

            Assert.Catch<ArgumentOutOfRangeException>(() => game.Guess("123a45"));


        }

        /// <summary>
        /// AddEntry_CheckIfNothingHappens_NothingHappens for 
        /// </summary>
        [Test]
        public void GuessGame_NonNumberString_ResultZero()
        {
            var game = new GuessGame();

            try
            {
                game.Guess("123a45");
            }
            catch
            {
                //ignore
            }

            Assert.That(game.Result, Is.Zero);
        }

        /// <summary>
        /// AddEntry_CheckIfNothingHappens_NothingHappens for 
        /// </summary>
        [Test]
        public void GuessGame_NumberHigher_MessagePassed()
        {
            var game = new GuessGame(100);
            string message = null;
            game.Messages += (sender, args) => message = args.Result;


            game.Guess("50");


            Assert.That(message, Is.EqualTo(Resource1.More).IgnoreCase);
        }

        [Test]
        public void Name_EuqalsGuessGame()
        {
            var game = new GuessGame();

            var name = game.Name;

            Assert.That(name, Is.EqualTo("Guess game").IgnoreCase);
        }
    }
}
