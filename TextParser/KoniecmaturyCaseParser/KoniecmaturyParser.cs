﻿using TextParser.AbstractParsers;

namespace KoniecmaturyCaseParser
{
    public class KoniecmaturyParser : GaderypolukiTypeParsers
    {
        public KoniecmaturyParser() : base("KO-NI-EC-MA-TU-RY")
        {
        }
    }
}