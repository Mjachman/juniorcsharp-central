﻿using Ninject.Modules;
using TextParser;

namespace PokemonCaseParser
{
    public class PokemonModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IParser>().To<PokemonParser>();
        }
    }
}
