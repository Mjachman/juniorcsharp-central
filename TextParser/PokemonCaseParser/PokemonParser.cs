﻿using System;
using System.Threading.Tasks;
using TextParser.AbstractParsers;

namespace PokemonCaseParser
{
    public class PokemonParser : CharParser
    {
        private readonly Random _rnd = new Random();

        public override string ToString()
        {
            return "PoKemOn";
        }

        protected override Task<char> ParseCharAsync(char c)
        {
            return Task.FromResult(_rnd.Next(2) == 0 ? char.ToUpper(c) : char.ToLower(c));
        }

        
    }
}
