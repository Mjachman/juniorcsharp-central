﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParser.AbstractParsers;

namespace GaderypolukiCaseParser
{
    public class GaderypolukiParser : GaderypolukiTypeParsers
    {
        public GaderypolukiParser() : base("GA-DE-RY-PO-LU-KI")
        {
        }
    }
}
