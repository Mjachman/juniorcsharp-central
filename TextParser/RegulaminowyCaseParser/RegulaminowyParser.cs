﻿using TextParser.AbstractParsers;

namespace RegulaminowyCaseParser
{
    public class RegulaminowyParser : GaderypolukiTypeParsers
    {
        public RegulaminowyParser() : base("RE-GU-LA-MI-NO-WY")
        {
        }
    }
}
