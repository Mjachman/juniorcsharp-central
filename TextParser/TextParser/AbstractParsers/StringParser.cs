﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace TextParser.AbstractParsers
{
    /// <summary>
    /// Parsees file based on it's content
    /// </summary>
    public abstract class StringParser : IParser
    {
        public async Task ParseFileAsync(string inputFile, string outputFile,CancellationToken ct = new CancellationToken())
        {
            ct.ThrowIfCancellationRequested();
            var input = await ReadAllTextAsync(inputFile);
            ct.ThrowIfCancellationRequested();
            var output = await ParseTextAsync(input, ct).ConfigureAwait(false);
            ct.ThrowIfCancellationRequested();
            await WriteAllTextAsync(outputFile, output);
        }
        

        public abstract Task<string> ParseTextAsync(string input, CancellationToken ct = new CancellationToken());

      

        protected Task<string> ReadAllTextAsync(string pathSource)
        {
            return Task.Run(() => File.ReadAllText(pathSource));
        }

        protected async Task WriteAllTextAsync(string pathDest, string output)
        {
            await Task.Run(() => File.WriteAllText(pathDest, output));
        }

        
    }
}


   