﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParser.AbstractParsers
{
    /// <summary>
    /// Parses file char-by-char, turning every char to another char
    /// </summary>
    public abstract class CharParser : StringParser
    {
        public override async Task<string> ParseTextAsync(string input, CancellationToken ct )
        {
            var sb = new StringBuilder();

            ct.ThrowIfCancellationRequested();

            foreach (var character in input)
            {
                sb.Append(await ParseCharAsync(character).ConfigureAwait(false));
                ct.ThrowIfCancellationRequested();

            }

            return await Task.Run(()=>sb.ToString(), ct);
        }

     
        protected abstract Task<char> ParseCharAsync(char input);

      
    }
}
