﻿using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParser.AbstractParsers
{
    public abstract class VigenereParser : StringParser
    {
        private readonly char[,] _alphabetTable = new char[26,26];

        protected VigenereParser()
        {
            CreateAlphabetTable();
        }

        protected abstract char[] PasswordArray { get; }

        public override async Task<string> ParseTextAsync(string input, CancellationToken ct = new CancellationToken())
        {
            int indexOfPassword = -1;
            var sb = new StringBuilder();
            foreach (var c in input)
            {
                ct.ThrowIfCancellationRequested();
                if (indexOfPassword == PasswordArray.Length - 1)
                    indexOfPassword = -1;

                sb.Append(char.IsLetter(c) ? _alphabetTable[c - 'a', PasswordArray[++indexOfPassword] - 'a'] : c);
            }
            return await Task.Run(() => sb.ToString(), ct);
        }

        private void CreateAlphabetTable()
        {
            var start = 'a';
            for (var i = 0; i < 26; i++)
            {
                var starttemp = start;

                for (var j = 0; j < 26; j++)
                {
                    if (starttemp == 'z')
                    {
                        _alphabetTable[i, j] = starttemp;
                        starttemp = 'a';
                        continue;
                    }
                    _alphabetTable[i, j] = starttemp;
                    starttemp++;
                }
                start++;
            }
        }
    }
}

