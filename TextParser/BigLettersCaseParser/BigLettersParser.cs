﻿using System.Threading.Tasks;
using TextParser.AbstractParsers;
using static System.Char;

namespace BigLettersCaseParser
{
    public class BigLettersParser : CharParser
    {
        public override string ToString()
        {
            return "Big Letter";
        }
        
        protected override async Task<char> ParseCharAsync(char input)
        {
              await  Task.Delay(100).ConfigureAwait(false);
              return ToUpper(input);
        }
    }
}