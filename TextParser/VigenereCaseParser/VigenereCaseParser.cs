﻿using TextParser.AbstractParsers;

namespace VigenereCaseParser
{
    public class VigenereCaseParser : VigenereParser
    {
        protected override char[] PasswordArray => new[] {'v', 'i', 'g', 'e', 'n', 'e', 'r', 'e'};

        public override string ToString()
        {
            return "Vigenere'a";
        }
    }
}